using Moq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;


namespace Otus.Teaching.PromoCodeFactory.UnitTests
{
	public class Configuration
	{

		public Mock<DbContext> _dbContextMock = new Mock<DbContext>();

		public static ServiceProvider GetServiceProvider()
		{
			var builder = new ConfigurationBuilder();
			var configuration = builder.Build();
			var serviceCollection = GetServiceCollection(configuration);
			var serviceProvider = serviceCollection.BuildServiceProvider();

			return serviceProvider;
		}

		public static IServiceCollection GetServiceCollection(IConfigurationRoot configuration)
		{
			var	serviceCollection = new ServiceCollection();

			serviceCollection.AddSingleton(configuration)
			.AddSingleton((IConfiguration)configuration)
			.AddScoped<IDbInitializer, EfDbInitializer>()
			.AddScoped(typeof(IRepository<>), typeof(EfRepository<>))
			.AddDbContext<DataContext>(x =>
			{
				x.UseInMemoryDatabase("InMemoryDb");
				x.UseSnakeCaseNamingConvention();
				x.UseLazyLoadingProxies();
			});

			return serviceCollection;
		}


	}
}