﻿using Xunit;
using FluentAssertions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{

    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private PartnerFactory partnerFactory = new PartnerFactory();
        private PartnersControllerFactory partnersControllerFactory = new PartnersControllerFactory();

        [Fact]
        public void SetPartnerPromoCode_ParterNotFound_ReturnNotFound()
        {
            // Arrage
            Partner partner = null;
            var partnersController = partnersControllerFactory
                .CreatePartnersController(PartnersControllerType.MOCK, partner); 

            Guid id = Guid.NewGuid();
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequest();

            // Act
            var result = partnersController.SetPartnerPromoCodeLimitAsync(id, request);

            // Assert
            Assert.NotNull(result);
            NotFoundResult actual = result.Result as NotFoundResult;
            Assert.NotNull(actual);
            Assert.Equal(404, actual.StatusCode);
        }
        [Fact]
        public void SetPartnerPromoCode_PartnerIsBlocked_ReturnBadRequest()
        {
            // Arrage
            var partner = partnerFactory.CreatePartner(PartnerType.BLOCKED_PARTNER);
            var partnersController = partnersControllerFactory
                .CreatePartnersController(PartnersControllerType.MOCK, partner);

            Guid id = Guid.NewGuid();
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequest();

            // Act
            var result = partnersController.SetPartnerPromoCodeLimitAsync(id, request);

            // Assert
            Assert.NotNull(result);
            BadRequestObjectResult actual = result.Result as BadRequestObjectResult;
            Assert.NotNull(actual);
            Assert.Equal(400, actual.StatusCode);
            Assert.Equal("Данный партнер не активен", actual.Value.ToString());
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void SetPartnerPromoCode_OnlyIfActiveLimitExists_NumberIssuedPromoCodesShouldBeCleared(bool hasActivelimit)
        {
            // Arrage
            Partner partner = hasActivelimit ?
                              partnerFactory.CreatePartner(PartnerType.PARTNER_WITH_ACTIVE_LIMIT) :
                              partnerFactory.CreatePartner(PartnerType.PARTNER_WITHOUT_ACTIVE_LIMIT);

            int start_NumberIssuedPromoCodes = partner.NumberIssuedPromoCodes;
            var partnersController = partnersControllerFactory
                .CreatePartnersController(PartnersControllerType.MOCK, partner);

            Guid id = Guid.NewGuid();
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 10,
                EndDate = DateTime.Now.AddDays(10)
            };

            // Act
            var result = partnersController.SetPartnerPromoCodeLimitAsync(id, request);

            // Assert
            Assert.NotNull(result);
            CreatedAtActionResult actual = result.Result as CreatedAtActionResult;
            Assert.NotNull(actual);
            Assert.Equal(201, actual.StatusCode);
            if (hasActivelimit)
            {
                Assert.Equal(0, partner.NumberIssuedPromoCodes);
            }
            else
            {
                Assert.Equal(start_NumberIssuedPromoCodes, partner.NumberIssuedPromoCodes);
            }
        }
        [Fact]
        public void SetPartnerPromoCode_LimitIsSet_PreviousLimitShouldBeClosed()
        {
            // Arrage
            var partner = partnerFactory.CreatePartner(PartnerType.PARTNER_WITH_ACTIVE_LIMIT);
            var partnersController = partnersControllerFactory
                .CreatePartnersController(PartnersControllerType.MOCK, partner);

            Guid id = Guid.NewGuid();
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 10,
                EndDate = DateTime.Now.AddDays(10)
            };

            // Act
            var result = partnersController.SetPartnerPromoCodeLimitAsync(id, request);

            // Assert
            Assert.NotNull(result);
            CreatedAtActionResult actual = result.Result as CreatedAtActionResult;
            Assert.NotNull(actual);
            Assert.Equal(201, actual.StatusCode);

            var activeLimit = (from i in partner.PartnerLimits
                               select i).FirstOrDefault();
            Assert.NotNull(activeLimit);
            Assert.NotNull(activeLimit.CancelDate);
            Assert.Equal(DateTime.Now.ToShortDateString(), activeLimit.CancelDate.Value.ToShortDateString());
        }

        [Fact]
        public void SetPartnerPromoCode_RequestLimitEqualOrLessZero_ReturnBadRequest()
        {
            // Arrage
            Partner partner = partnerFactory.CreatePartner(PartnerType.PARTNER_WITH_ACTIVE_LIMIT);
            var partnersController = partnersControllerFactory
                .CreatePartnersController(PartnersControllerType.MOCK, partner);

            Guid id = Guid.NewGuid();
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 0,
                EndDate = DateTime.Now.AddDays(10)
            };

            // Act
            var result = partnersController.SetPartnerPromoCodeLimitAsync(id, request);

            // Assert
            Assert.NotNull(result);
            BadRequestObjectResult actual = result.Result as BadRequestObjectResult;
            Assert.NotNull(actual);
            Assert.Equal(400, actual.StatusCode);
            Assert.Equal("Лимит должен быть больше 0", actual.Value.ToString());
        }

        [Fact]
        public void SetPartnerPromoCode_LimitRequestIsCorrect_NewLimitShouldBeSAvedInDb()
        {
            // Arrage
            var serviceProvider = Configuration.GetServiceProvider();
            var partnersController = partnersControllerFactory
                .CreatePartnersController(PartnersControllerType.IOC, serviceProvider: serviceProvider);

            Guid id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 100,
                EndDate = DateTime.Now.AddDays(10)
            };

            // Act
            var result = partnersController.SetPartnerPromoCodeLimitAsync(id, request);

            // Assert
            Assert.NotNull(result);
            result.Result.Should().BeAssignableTo<CreatedAtActionResult>();

            DataContext context = serviceProvider.GetRequiredService<DataContext>();
            Assert.NotNull(context);
            
            var partner = context.Set<Partner>().FirstOrDefault(x => x.Id == id);
            Assert.NotNull(context);
            Assert.Equal(2, partner.PartnerLimits.Count);

            var lastLimit = partner.PartnerLimits.LastOrDefault();
            Assert.Equal(request.Limit, lastLimit.Limit);
            Assert.Equal(request.EndDate.ToShortDateString(), lastLimit.EndDate.ToShortDateString());

        }

    }
}
