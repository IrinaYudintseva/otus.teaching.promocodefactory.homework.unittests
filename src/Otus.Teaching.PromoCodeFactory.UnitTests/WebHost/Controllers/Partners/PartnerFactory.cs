﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public enum PartnerType
    {
        BLOCKED_PARTNER,
        PARTNER_WITH_ACTIVE_LIMIT,
        PARTNER_WITHOUT_ACTIVE_LIMIT
    }
    public class BlockedPartrer : Partner 
    {
        public BlockedPartrer() 
        {
            this.Id = Guid.NewGuid();
            this.IsActive = false;
        }
    };

    public class PartrerWithActiveLimit : Partner 
    {
        public PartrerWithActiveLimit() 
        {
            Id = Guid.NewGuid();
            IsActive = true;
            NumberIssuedPromoCodes = 10;
            PartnerLimits = new List<PartnerPromoCodeLimit> 
            {
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.NewGuid(),
                    CreateDate = DateTime.Now.AddDays(-5),
                    Limit = 100,
                    PartnerId = this.Id,
                    Partner = this
                }
            };
        }
    };
    public class PartrerWithoutActiveLimit : Partner 
    {
        public PartrerWithoutActiveLimit()
        {
            Id = Guid.NewGuid();
            IsActive = true;
            NumberIssuedPromoCodes = 10;
            PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.NewGuid(),
                    CreateDate = DateTime.Now.AddDays(-5),
                    CancelDate = DateTime.Now.AddDays(5),
                    Limit = 100,
                    PartnerId = this.Id,
                    Partner = this
                }
            };
        }
    };

    public class PartnerFactory
    {
        public Partner CreatePartner(PartnerType type)
        {
            Partner partner = null;
            switch (type)
            {
                case PartnerType.BLOCKED_PARTNER:
                    partner = new BlockedPartrer();
                    break;
                case PartnerType.PARTNER_WITH_ACTIVE_LIMIT:
                    partner = new PartrerWithActiveLimit();
                    break;
                case PartnerType.PARTNER_WITHOUT_ACTIVE_LIMIT:
                    partner = new PartrerWithoutActiveLimit();
                    break;
            }
            return partner;
        }
    }
}