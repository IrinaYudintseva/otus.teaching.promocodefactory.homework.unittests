﻿using Moq;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public enum PartnersControllerType
    {
        MOCK,
        IOC
    }
    public class PartnersControllerFactory
    {
        public PartnersController CreatePartnersController(PartnersControllerType type, Partner testingPartner = null, ServiceProvider serviceProvider = null)
        {
            PartnersController partnersController = null;
            switch (type)
            {
                case PartnersControllerType.MOCK:
                    Mock<IRepository<Partner>> _partnersRepositoryMock = new Mock<IRepository<Partner>>();
                    _partnersRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(testingPartner);
                    _partnersRepositoryMock.Setup(m => m.UpdateAsync(It.IsAny<Partner>()));

                    partnersController = new PartnersController(_partnersRepositoryMock.Object); ;

                    break;
                case PartnersControllerType.IOC:

                    if(serviceProvider == null)
                        serviceProvider = Configuration.GetServiceProvider();
                    
                    IDbInitializer initializer = serviceProvider.GetRequiredService<IDbInitializer>();
                    initializer.InitializeDb();

                    IRepository<Partner> partnersRepository = serviceProvider.GetRequiredService<IRepository<Partner>>();
                    partnersController = new PartnersController(partnersRepository);

                    break;
            }
            return partnersController;
        }
    }
}